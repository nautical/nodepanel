/**
 * GET /
 * Home page.
 */

exports.index = function(req, res) {
  var date = new Date();
  var year = date.getFullYear();
  res.render('home', {
    title: 'Home',
    year : year
  });
};
